const angular = require.resolve('.rulesets/angular');
const rxjs = require.resolve('./rulesets/rxjs');
const typescript = require.resolve('./rulesets/typescript');

module.exports = {
    angular,
    rxjs,
    typescript
}
