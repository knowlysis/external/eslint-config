module.exports = {
  "extends": [
    "plugin:rxjs/recommended"
  ],
  "rules": {
    "rxjs/no-compat": "error",
    "rxjs/no-ignored-error": "error",
    "rxjs/no-implicit-any-catch": "off",
    "rxjs/no-subject-value": "off"
  }
}
